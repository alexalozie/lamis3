/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.dao.jdbc.LabtestJDBC;
import org.fhi360.lamis.exchange.ndr.schema.AnswerType;
import org.fhi360.lamis.exchange.ndr.schema.CodedSimpleType;
import org.fhi360.lamis.exchange.ndr.schema.ConditionType;
import org.fhi360.lamis.exchange.ndr.schema.LaboratoryOrderAndResult;
import org.fhi360.lamis.exchange.ndr.schema.LaboratoryReportType;
import org.fhi360.lamis.exchange.ndr.schema.NumericType;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.StringUtil;

/**
 *
 * @author user1
 */
public class LaboratoryReportTypeMapper {   
    private JDBCUtil jdbcUtil;
    
    public LaboratoryReportTypeMapper () {
    }
    
    public ConditionType laboratoryReportType(long patientId, ConditionType condition) {
        try {
            LaboratoryReportType laboratory = null;
            String dateReported = "";

            String query = "SELECT DISTINCT laboratory_id, date_collected, date_reported, labtest_id, resultab, resultpc FROM lab WHERE (resultab != '' OR resultpc != '') AND patient_id = " + patientId + " ORDER BY date_reported";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                //If dateReported is not equal to date_reported, add this laboratory report to conditionType
                //ie if dateReported is before date_reported return < 0 OR if dateReported is after date_reported return > 0
                //If dateReported is equal to date_reported return 0
                if(!dateReported.equals(DateUtil.parseDateToString(rs.getDate("date_reported"), "yyyy-MM-dd"))) {
                    if(laboratory != null) {
                        condition.getLaboratoryReport().add(laboratory);
                        System.out.println("Lab test report added....");
                    }
                    //Any time the date changes reset the dateVisit variable
                    dateReported = DateUtil.parseDateToString(rs.getDate("date_reported"), "yyyy-MM-dd");
                    //Instantiate a new laboratory report for each date
                    laboratory = new LaboratoryReportType();                    
                    laboratory.setVisitID(Long.toString(rs.getLong("laboratory_id")));   
                    laboratory.setVisitDate(DateUtil.getXmlDate(rs.getDate("date_collected")));
                    laboratory.setLaboratoryTestIdentifier("0000001");
                }
                               
                long labtestId = rs.getLong("labtest_id");
                String description = LabtestJDBC.getLabtest(labtestId);
                CodedSimpleType cst = CodeSetResolver.getCodedSimpleType("LAB_RESULTED_TEST", description);
                if(cst != null && laboratory != null) {
                    String resultab = rs.getString("resultab") == null ? "" : rs.getString("resultab");                 
                    String resultpc = rs.getString("resultpc") == null ? "" : rs.getString("resultpc");

                    if(!resultab.isEmpty() || !resultpc.isEmpty()) {
                        //Set the NDR code & description for this lab test
                        LaboratoryOrderAndResult result = new LaboratoryOrderAndResult();
                        result.setLaboratoryResultedTest(cst); 
                        result.setOrderedTestDate(DateUtil.getXmlDate(rs.getDate("date_collected")));
                        result.setResultedTestDate(DateUtil.getXmlDate(rs.getDate("date_reported")));

                        //Set the lab test result values either numeric or text
                        AnswerType answer = new AnswerType();
                        NumericType numeric = new NumericType();
                        String value = !resultab.isEmpty()? resultab : resultpc;
                        if(StringUtil.isInteger(value)) { 
                            Double d = Double.valueOf(StringUtil.stripCommas(value));
                            numeric.setValue1(d.intValue());
                            answer.setAnswerNumeric(numeric);
                        }
                        else {
                            if(labtestId == 16) {
                                numeric.setValue1(0);  //if lab test is a viralload set the value to 0 
                                answer.setAnswerNumeric(numeric);
                            }
                            else {
                                answer.setAnswerText(value);
                            }
                        }
                        result.setLaboratoryResult(answer);		
                        laboratory.getLaboratoryOrderAndResult().add(result); 
                    }                    
                }
            }
            //Add the last lab test result in the resultSet to condition
            if(laboratory != null) {
                condition.getLaboratoryReport().add(laboratory);
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return condition;            
    }

}
