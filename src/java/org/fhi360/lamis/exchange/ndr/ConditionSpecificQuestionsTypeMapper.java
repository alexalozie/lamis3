/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.exchange.ndr.schema.CodedSimpleType;
import org.fhi360.lamis.exchange.ndr.schema.ConditionSpecificQuestionsType;
import org.fhi360.lamis.exchange.ndr.schema.HIVQuestionsType;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;

/**
 *
 * @author user1
 */
public class ConditionSpecificQuestionsTypeMapper {
    private JDBCUtil jdbcUtil;
    
    public ConditionSpecificQuestionsTypeMapper() {
    }
    
    public ConditionSpecificQuestionsType conditionSpecificQuestionsType(long patientId) {
	ConditionSpecificQuestionsType disease = new ConditionSpecificQuestionsType();
        HIVQuestionsType hiv = new HIVQuestionsType();
        String databaseName = "h2"; //new PropertyAccessor().getDatabaseName();

        int age = 0;
        try {
            //These are HIV question relating to registration
            String query = "SELECT TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age, date_registration, status_registration, current_status, date_current_status, date_started FROM entity WHERE patient_id = " + patientId;
            if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DATEDIFF(YEAR, date_birth, CURDATE()) AS age, date_registration, status_registration, current_status, date_current_status, date_started FROM entity WHERE patient_id = " + patientId;

            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                String statusRegistration = rs.getString("status_registration") == null ? "" : rs.getString("status_registration");
                String currentStatus = rs.getString("current_status") == null ? "" : rs.getString("current_status");
                String ARTStatus = rs.getDate("date_started") == null? "Pre-ART" : "ART";
                age = rs.getInt("age");
                
                //hiv.setCareEntryPoint("2");  Entry point is not captured in LAMIS
                if(statusRegistration.equalsIgnoreCase("HIV+ non ART")) hiv.setFirstConfirmedHIVTestDate(DateUtil.getXmlDate(rs.getDate("date_registration")));                
                if(statusRegistration.equalsIgnoreCase("Transfer In")) hiv.setTransferredInDate(DateUtil.getXmlDate(rs.getDate("date_registration")));                 
                
                if(currentStatus.contains("Transfer Out")) {
                    String status = CodeSetResolver.getCode("ART_STATUS", ARTStatus);
                    if(!status.isEmpty()) hiv.setTransferredOutStatus(status);
                    hiv.setTransferredOutDate(DateUtil.getXmlDate(rs.getDate("date_current_status")));
                    hiv.setPatientTransferredOut(true);                    
                }
                else {
                    hiv.setPatientTransferredOut(false);   
                }		
                if(currentStatus.equalsIgnoreCase("Known Death")) {
                    String status = CodeSetResolver.getCode("ART_STATUS", ARTStatus);
                    if(!status.isEmpty())  hiv.setStatusAtDeath(status);
                    hiv.setDeathDate(DateUtil.getXmlDate(rs.getDate("date_current_status")));
                    hiv.setPatientHasDied(true);
                }
                else {
                    hiv.setPatientTransferredOut(false);   
                }
                if(statusRegistration.equalsIgnoreCase("HIV+ non ART")) hiv.setEnrolledInHIVCareDate(DateUtil.getXmlDate(rs.getDate("date_registration")));
            }   
                     
            //These are HIV question ART commencement
            query = "SELECT * FROM visit WHERE patient_id = " + patientId + " AND commence = 1";
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
            if(rs.next()) {                
                String eligible = "";
                int cd4 = (int) rs.getDouble("cd4");
                int cd4p = (int) rs.getDouble("cd4p");
                String clinicStage = rs.getString("clinic_stage") == null ? "" : rs.getString("clinic_stage");                
                String funcStatus = rs.getString("func_status") == null ? "" : rs.getString("func_status");
                String regimen = rs.getString("regimen") == null ? "" : rs.getString("regimen");
             
                if(age >= 15) {
                    if(cd4 < 350) {
                        eligible = CodeSetResolver.getCode("WHY_ELIGIBLE", "CD4"); 
                    }
                    else {
                        if(clinicStage.equalsIgnoreCase("Stage III") || clinicStage.equalsIgnoreCase("Stage IV")) {
                            eligible = CodeSetResolver.getCode("WHY_ELIGIBLE", "Staging"); 
                        }
                        else {
                            //Information not available
                        }
                    }
                }
                else {
                    if(cd4 < 750 || cd4p < 25) {
                        eligible = cd4 < 25? CodeSetResolver.getCode("WHY_ELIGIBLE", "CD4p") : CodeSetResolver.getCode("WHY_ELIGIBLE", "CD4");                           
                    }
                    else {
                        if(clinicStage.equalsIgnoreCase("Stage III") || clinicStage.equalsIgnoreCase("Stage IV")) {
                            eligible = CodeSetResolver.getCode("WHY_ELIGIBLE", "Staging"); 
                        }
                        else {
                            //Information not available
                        }
                    }                    
                }
                hiv.setARTStartDate(DateUtil.getXmlDate(rs.getDate("date_visit")));
                hiv.setMedicallyEligibleDate(DateUtil.getXmlDate(rs.getDate("date_visit")));
                if(!eligible.isEmpty()) hiv.setReasonMedicallyEligible(eligible);
                if(!clinicStage.isEmpty()) {
                    clinicStage = CodeSetResolver.getCode("WHO_STAGE", clinicStage);
                    if(!clinicStage.isEmpty()) hiv.setWHOClinicalStageARTStart(clinicStage);
                }
	        if(!funcStatus.isEmpty()) {
                    funcStatus = CodeSetResolver.getCode("FUNCTIONAL_STATUS", funcStatus);
                    if(!funcStatus.isEmpty())  hiv.setFunctionalStatusStartART(funcStatus);
                }
		if(!regimen.isEmpty()) {
                    CodedSimpleType cst = CodeSetResolver.getRegimen(regimen);
                    if(cst != null) hiv.setFirstARTRegimen(cst);
                }
                
                int weight = (int) rs.getDouble("body_weight");
                int height = (int) rs.getDouble("height");
                if(weight > 0) hiv.setWeightAtARTStart(weight);
                if(height > 0) hiv.setChildHeightAtARTStart(height);
                if(cd4 > 0) {
                    hiv.setCD4AtStartOfART(Integer.toString(cd4)); //Long.toString(Math.round(rs.getDouble("cd4")))
                }
                else {
                    if(cd4p > 0) hiv.setCD4AtStartOfART(Integer.toString(cd4p));
                }
            }              
            disease.setHIVQuestions(hiv);
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return disease;                
    }
    
}


//
//double d = 9.5;
//int i = (int)d;
////i = 9
//
//Double D = 9.5;
//int i = Integer.valueOf(D.intValue());
////i = 9
//
//double d = 9.5;
//Long L = Math.round(d);
//int i = Integer.valueOf(L.intValue());
////i = 10