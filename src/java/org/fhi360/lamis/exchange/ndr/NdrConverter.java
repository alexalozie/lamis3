package org.fhi360.lamis.exchange.ndr;

import java.io.File;
import java.util.Date;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

 
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.exchange.ndr.schema.AddressType;
import org.fhi360.lamis.exchange.ndr.schema.CommonQuestionsType;
import org.fhi360.lamis.exchange.ndr.schema.ConditionSpecificQuestionsType;
import org.fhi360.lamis.exchange.ndr.schema.ConditionType;
import org.fhi360.lamis.exchange.ndr.schema.Container;
import org.fhi360.lamis.exchange.ndr.schema.EncountersType;
import org.fhi360.lamis.exchange.ndr.schema.IndividualReportType;
import org.fhi360.lamis.exchange.ndr.schema.MessageHeaderType;
import org.fhi360.lamis.exchange.ndr.schema.PatientDemographicsType;
import org.fhi360.lamis.exchange.ndr.schema.ProgramAreaType;
import org.fhi360.lamis.utility.FileUtil;
import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.sql.Timestamp;
import java.util.Map;
import org.fhi360.lamis.dao.jdbc.FacilityJDBC;
import org.fhi360.lamis.service.UploadFolderService;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 * 
 * @author Stephen Macauley, InductiveHealth Informatics (stephen@inductivehealth.com)
 * 
 * <p>
 * This class will generate a sample NDR message using the NDR Schema and sample data
 * 
 * This class is intended provide an <i>example</i> of how to utilize the Java Architecture for XML Binding (JAXB) 
 * Application Programming Interface (API) [http://www.oracle.com/technetwork/articles/javase/index-140168.html]
 * to bind data based on the NDR Schema to generate a validate XML message.
 *
 * The data utilized in this example if fictional.
 * 
 * Test client implemented using jdk1.6.0_14 using Eclipse Java EE IDE for Web Developers 
 * using Dali Java Persistence Tools - JAXB Support Version 1.5.2.v201501171820 for the JAXB implementation
 * 
 * </p>
 * 
 *  */

public class NdrConverter {
    private JDBCUtil jdbcUtil;
       
    private final String contextPath;
    private final PatientDemographicsMapper patientDemographicsMapper;
    private final AddressTypeMapper addressTypeMapper;
    private final CommonQuestionsTypeMapper commonQuestionsTypeMapper;
    private final ConditionSpecificQuestionsTypeMapper conditionSpecificQuestionsTypeMapper;
    private final EncountersTypeMapper encountersTypeMapper;
    private final MessageHeaderTypeMapper messageHeaderTypeMapper;
    private final LaboratoryReportTypeMapper laboratoryReportTypeMapper;
    private final RegimenTypeMapper regimenTypeMapper;
    
    private long messageId;
    private String statusCode;
    
    public NdrConverter() {
        this.contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        this.messageHeaderTypeMapper = new MessageHeaderTypeMapper();
        this.patientDemographicsMapper = new PatientDemographicsMapper();
        this.addressTypeMapper = new AddressTypeMapper();
        this.commonQuestionsTypeMapper = new CommonQuestionsTypeMapper();
        this.conditionSpecificQuestionsTypeMapper = new ConditionSpecificQuestionsTypeMapper();
        this.encountersTypeMapper = new EncountersTypeMapper();
        this.regimenTypeMapper = new RegimenTypeMapper();
        this.laboratoryReportTypeMapper = new LaboratoryReportTypeMapper();
    } 

    public synchronized void buildMessage(String query, boolean batch) {
        this.messageId = MessageStatus.getLastMessageId();
        
        FileUtil fileUtil = new FileUtil();
        UploadFolderService uploadFolderService = new UploadFolderService();
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                long facilityId = rs.getLong("facility_id");
                String folder = contextPath + "transfer/temp/" + Long.toString(facilityId) + "/";  
                fileUtil.makeDir(folder);        
                System.out.println("NDR messaging....."+facilityId);
                if(uploadFolderService.getUploadFolderStatus(folder).equalsIgnoreCase("unlocked")) {
                    fileUtil.deleteFileWithExtension(folder, ".xml");
                    fileUtil.deleteFileWithExtension(folder, ".zip");
                    uploadFolderService.lockUploadFolder(folder); 
                    buildMessage(facilityId);
                    uploadFolderService.unlockUploadFolder(folder); 
                }
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }
        
    public String buildMessage(long facilityId) {
        executeUpdate("DROP INDEX IF EXISTS idx_entity");                       
        executeUpdate("DROP TABLE IF EXISTS entity");                       
        executeUpdate("CREATE TEMPORARY TABLE entity AS SELECT * FROM patient WHERE hospital_num != '' AND facility_id = " + facilityId);
        executeUpdate("CREATE INDEX idx_entity ON entity(patient_id)");

        executeUpdate("DROP INDEX IF EXISTS idx_visit");                       
        executeUpdate("DROP TABLE IF EXISTS visit");        
        executeUpdate("CREATE TEMPORARY TABLE visit AS SELECT * FROM clinic WHERE facility_id = " + facilityId);
        executeUpdate("CREATE INDEX idx_visit ON visit(patient_id)");

        executeUpdate("DROP INDEX IF EXISTS idx_pharm");                       
        executeUpdate("DROP TABLE IF EXISTS pharm");        
        executeUpdate("CREATE TEMPORARY TABLE pharm AS SELECT * FROM pharmacy WHERE facility_id = " + facilityId);
        executeUpdate("CREATE INDEX idx_pharm ON pharm(patient_id)");

        executeUpdate("DROP INDEX IF EXISTS idx_lab");                       
        executeUpdate("DROP TABLE IF EXISTS lab");        
        executeUpdate("CREATE TEMPORARY TABLE lab AS SELECT * FROM laboratory WHERE facility_id = " + facilityId);
        executeUpdate("CREATE INDEX idx_lab ON lab(patient_id)");
        
        executeUpdate("DROP INDEX IF EXISTS idx_msg");                       
        executeUpdate("DROP TABLE IF EXISTS msg");        
        executeUpdate("CREATE TEMPORARY TABLE msg AS SELECT patient_id, MAX(time_stamp) AS last_message FROM ndrmessagelog WHERE facility_id = " + facilityId + " GROUP BY patient_id");
        executeUpdate("CREATE INDEX idx_msg ON msg(patient_id)");

        String query = "SELECT DISTINCT patient_id FROM entity WHERE facility_id = " + facilityId;
        ResultSet resultSet = executeQuery(query);
        try {
            int rowcount = 0;
            while(resultSet.next()) {
                rowcount++;
                long patientId = resultSet.getLong("patient_id");
                
                //Get the last message status
                Map messageStatus = MessageStatus.getMessageStatus(patientId);
                statusCode = (String) messageStatus.get("statusCode");
                
                //Get the last time this patient message was generated
                Timestamp lastMessage = (Timestamp) messageStatus.get("lastMessage");
                if(lastMessage == null) {
                    buildMessage(facilityId, patientId, statusCode);
                }
                else {
                    query = "SELECT DISTINCT patient_id FROM entity WHERE patient_id = " + patientId + " AND time_stamp > '" + lastMessage + "' UNION SELECT DISTINCT patient_id FROM visit WHERE  patient_id = " + patientId + " AND time_stamp > '" + lastMessage + "' UNION SELECT DISTINCT patient_id FROM pharm WHERE patient_id = " + patientId + " AND time_stamp > '" + lastMessage + "' UNION SELECT DISTINCT patient_id FROM lab WHERE patient_id = " + patientId + " AND time_stamp > '" + lastMessage + "'";
                    ResultSet rs = executeQuery(query);
                    if(rs.next()) {
                        if(rs.getLong("patient_id") != 0L) buildMessage(facilityId, patientId, statusCode);
                    }                 
               }
               System.out.println("Patient ID..."+patientId+"....Rec...."+rowcount);                
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return zipFiles(facilityId);
    }

    
    public String buildMessage(String csvFile) {
        String[] row = null;
        int rowcount = 0;
        try {
            if(new File(csvFile).exists()) {
                CSVReader csvReader = new CSVReader(new FileReader(csvFile));
                while((row = csvReader.readNext()) != null) {
                    rowcount++;
                    if (rowcount > 1 ) {
                       String fileName = row[0];
                       System.out.println("File name..."+fileName+"....Rec...."+rowcount);  
                       
                       String query = "SELECT patient_id, facility_id FROM ndrmessagelog WHERE file_name = '" + fileName + "'";
                       
                       ResultSet resultSet = executeQuery(query);
                       try {
                           while(resultSet.next()) {
                              long patientId = resultSet.getLong("patient_id");
                              long facilityId = resultSet.getLong("facility_id");
                              
                              //Get the last message status
                              Map messageStatus = MessageStatus.getMessageStatus(patientId);
                              String statusCode = (String) messageStatus.get("statusCode");                       
                              buildMessage(facilityId, patientId, statusCode);
                              System.out.println("Patient ID..."+patientId+"....Rec...."+rowcount);                                
                           }
                       }
                       catch (Exception exception) {
                           exception.printStackTrace();
                       }          
                   }
                }
                csvReader.close();                        
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return ""; //zipFiles();
    }
    
    //This method builds the xml message for a patient. The last message variable determines what records in clinic encounters, drug refills and laboratory investigations should be included in the messages.
    private void buildMessage(long facilityId, long patientId, String statusCode) { 
        ///throws JAXBException, SAXException, DatatypeConfigurationException
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance("org.fhi360.lamis.exchange.ndr.schema");

            //Represents the Container (highest level of the schema)
            Container container = new Container();

            //Set the Header Information
            MessageHeaderType header = messageHeaderTypeMapper.messageHeaderType(patientId);
            header.setMessageStatusCode(statusCode);
            header.setMessageUniqueID(Long.toString(messageId++));

            //Set the Header to the Container
            container.setMessageHeader(header);

            //Create the Individual Report
            IndividualReportType individual = new IndividualReportType();

            //Patient Demographics
            PatientDemographicsType patient = patientDemographicsMapper.patientDemograhics(patientId);
            individual.setPatientDemographics(patient);

            //Condition
            ConditionType condition = new ConditionType();
            condition.setConditionCode(CodeSetResolver.getCode("CONDITION_CODE", "HIV_CODE"));
            ProgramAreaType programArea = new ProgramAreaType();
            programArea.setProgramAreaCode(CodeSetResolver.getCode("PROGRAM_AREA","HIV"));
            condition.setProgramArea(programArea);

            //Address
            AddressType address = addressTypeMapper.addressType(patientId);
            if(address != null) condition.setPatientAddress(address);

            //Common Questions
            CommonQuestionsType common = commonQuestionsTypeMapper.commonQuestionsType(patientId);
            if(common != null)  condition.setCommonQuestions(common);

            //HIV Specific
            ConditionSpecificQuestionsType disease = conditionSpecificQuestionsTypeMapper.conditionSpecificQuestionsType(patientId);
            if(disease != null)  condition.setConditionSpecificQuestions(disease);

            //Encounters
            EncountersType encounter = encountersTypeMapper.encounterType(patientId);
            if(encounter != null)  condition.setEncounters(encounter);

            //Populate ConditionType with laboratory report                
            condition = laboratoryReportTypeMapper.laboratoryReportType(patientId, condition);  

            //Populate ConditionType with regimen 
            condition = regimenTypeMapper.regimenType(patientId, condition);
            
            /**
            //Immunizations
            ImmunizationType immunization = new ImmunizationTypeResolver().immunizationType(patientId);
            if(immunization != null) condition.getImmunization().add(immunization);

            //Contacts
            ContactType contact = new ContactTypeResolver().contactType(patientId);
            if(contact != null) condition.getContact().add(contact);
             **/

            //Set the Condition to Individual
            //And finally set the individual to the Container
            individual.getCondition().add(condition);
            container.setIndividualReport(individual);

            //Validate Message Against NDR Schema (Version 1.2)
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
            Schema schema = sf.newSchema(new File("NDR 1.2.xsd")); 

            jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

            jaxbMarshaller.setSchema(schema);

            //Call Validator class to perform the validation		
            jaxbMarshaller.setEventHandler(new Validator());

            Thread.sleep(1000);     //Delay for some milli seconds 
            Date date = new Date() ;
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy") ;
            SimpleDateFormat dateForma2t = new SimpleDateFormat("HHmmss.ms") ;

            String fileName =  header.getMessageSendingOrganization().getFacilityID() + "_" + dateFormat.format(date) + "_" + dateForma2t.format(date) +  ".xml";
            File file = new File(contextPath + "transfer/temp/" + Long.toString(facilityId) + "/" + fileName) ;

            jaxbMarshaller.marshal(container, file);

            //Log the particulars of message generated into the message log table             
            String query = "INSERT INTO ndrmessagelog(message_id, patient_id, facility_id, file_name, time_stamp) VALUES(" + messageId + ", " + patientId + ", " + facilityId + ", '" + fileName + "', NOW())";        
            if(statusCode.equalsIgnoreCase("UPDATED")) query = "UPDATE ndrmessagelog SET facility_id = " + facilityId + ", file_name = '" + fileName + "', time_stamp = NOW() WHERE patient_id = " + patientId;
            executeUpdate(query);            
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }          
    }
    
    private String zipFiles(long facilityId) {
        String facilityName = FacilityJDBC.getFacilityName(facilityId);
        System.out.println("Facility name....."+facilityName);
        long timespamp = new Date().getTime();
        String fileName = facilityName.trim()+"_"+timespamp+".zip";
        System.out.println("filename:" + fileName);

        try {
            //Zip extracted NDR messages 
            String sourceFolder = contextPath + "transfer/temp/" + Long.toString(facilityId) + "/";
            String outputZipFile = contextPath + "transfer/ndr/"+fileName;

            FileUtil fileUtil = new FileUtil();
            fileUtil.deleteFileWithExtension(sourceFolder, ".ser");
            fileUtil.zipFolderContent(sourceFolder, outputZipFile); 
            
            //for servlets in the default(root) context, copy file to the transfer folder in root 
            fileUtil.makeDir(ServletActionContext.getRequest().getContextPath()+"/transfer/ndr/");
            if(!contextPath.equalsIgnoreCase(ServletActionContext.getRequest().getContextPath())) fileUtil.copyFile(fileName, contextPath+"transfer/ndr/", ServletActionContext.getRequest().getContextPath()+"/transfer/ndr/");                    
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }        
        return "transfer/ndr/"+fileName;
    }
    
    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }        
    }            

    private ResultSet executeQuery(String query) {
        ResultSet rs = null;
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return rs;
    }            

    private String zerorize(String messageId) {
        String zeros = "";
        int MAX_LENGTH = 8;
        if(messageId.length() < MAX_LENGTH) {
            for(int i = 0; i < MAX_LENGTH-messageId.length(); i++) {
                zeros = zeros + "0";  
            }
        }
        return zeros+messageId;
    }
    
}
