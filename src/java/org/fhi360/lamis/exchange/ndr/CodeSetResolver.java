/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

/**
 *
 * @author user1
 */
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.exchange.ndr.schema.CodedSimpleType;
import org.fhi360.lamis.exchange.ndr.schema.FacilityType;
import org.fhi360.lamis.service.beans.ContextProvider;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

public class CodeSetResolver {

    private final static TransactionTemplate transactionTemplate = ContextProvider.getBean(TransactionTemplate.class);

    private final static JdbcTemplate jdbcTemplate = ContextProvider.getBean(JdbcTemplate.class);

    public CodeSetResolver() {
    }

    public static CodedSimpleType getRegimen(String regimen) {
        CodedSimpleType[] cst = {new CodedSimpleType()};

        String reg[] = {regimen};
        try {
            String query[] = {"SELECT composition, regimentype_id FROM regimen WHERE description = '" + reg[0] + "'"};
            jdbcTemplate.query(query[0], (resulSet) -> {
                if (resulSet.next()) {
                    String regimentypeId = Long.toString(resulSet.getLong("regimentype_id"));
                    reg[0] = resulSet.getString("composition").trim() + "_" + regimentypeId;
                    query[0] = "SELECT * FROM ndrcodeset WHERE sys_description = '" + reg[0] + "'";
                    boolean[] found = {false};
                    jdbcTemplate.query(query[0], (rs) -> {
                        found[0] = true;
                        while (rs.next()) {
                            cst[0].setCode(rs.getString("code"));
                            cst[0].setCodeDescTxt(rs.getString("code_description"));
                        }
                        return null; //To change body of generated lambdas, choose Tools | Templates.
                    });
                    if (!found[0]) {
                        reg[0] = "Others_" + regimentypeId;
                        query[0] = "SELECT * FROM ndrcodeset WHERE sys_description = '" + reg[0] + "'";
                        jdbcTemplate.query(query[0], (rs1) -> {
                            found[0] = true;
                            while (rs1.next()) {
                                cst[0].setCode(rs1.getString("code"));
                                cst[0].setCodeDescTxt(rs1.getString("code_description"));
                            }
                            return true;
                        });
                        if (!found[0]) {
                            cst[0] = null;
                        }
                    }
                } else {
                    cst[0] = null;
                }
                return null;
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return cst[0];
    }

    public static CodedSimpleType getRegimenById(long regimenId) {
        CodedSimpleType[] cst = {new CodedSimpleType()};
        Long[] regim = {regimenId};
        try {
            String query[] = {"SELECT composition, regimentype_id FROM regimen WHERE regimen_id = " + regim[0]};
            jdbcTemplate.query(query[0], (resultSet) -> {
                if (resultSet.next()) {
                    String regimentypeId = Long.toString(resultSet.getLong("regimentype_id"));
                    String regimen[] = {resultSet.getString("composition").trim() + "_" + regimentypeId};

                    query[0] = "SELECT * FROM ndrcodeset WHERE sys_description = '" + regimen[0] + "'";
                    boolean[] found = {false};
                    jdbcTemplate.query(query[0], (rs) -> {
                        found[0] = true;
                        while (rs.next()) {
                            cst[0].setCode(rs.getString("code"));
                            cst[0].setCodeDescTxt(rs.getString("code_description"));
                        }
                        return null; //To change body of generated lambdas, choose Tools | Templates.
                    });
                    if (!found[0]) {
                        regimen[0] = "Others_" + regimentypeId;
                        query[0] = "SELECT * FROM ndrcodeset WHERE sys_description = '" + regimen[0] + "'";
                        jdbcTemplate.query(query[0], (rs1) -> {
                            found[0] = true;
                            while (rs1.next()) {
                                cst[0].setCode(rs1.getString("code"));
                                cst[0].setCodeDescTxt(rs1.getString("code_description"));
                            }
                            return null;
                        });
                        if (!found[0]) {
                            cst[0] = null;
                        }
                    }

                } else {
                    cst[0] = null;
                }
                return null; //To change body of generated lambdas, choose Tools | Templates.
            });

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return cst[0];
    }

    public static CodedSimpleType getCodedSimpleType(String codeSetNm, String description) {
        CodedSimpleType[] cst = {new CodedSimpleType()};
        try {
            String query = "SELECT * FROM ndrcodeset WHERE code_set_nm = '" + codeSetNm + "' AND sys_description = '" + description + "'";
            if (codeSetNm.contains(",")) {
                codeSetNm = codeSetNm.replace(",", "', '");
                codeSetNm = "'" + codeSetNm + "'";
                query = "SELECT * FROM ndrcodeset WHERE code_set_nm IN (" + codeSetNm + ") AND sys_description = '" + description + "'";
            }
            boolean[] found = {false};
            jdbcTemplate.query(query, (rs) -> {
                found[0] = true;
                while (rs.next()) {
                    cst[0].setCode(rs.getString("code"));
                    cst[0].setCodeDescTxt(rs.getString("code_description"));
                }
                return true;
            });
            if (!found[0]) {
                cst[0] = null;
            }

        } catch (Exception exception) {
            exception.printStackTrace();

        }
        return cst[0];
    }

    public static String getCode(String codeSetNm, String description) {
        String id[] = {""};
        try {
            String query = "SELECT * FROM ndrcodeset WHERE code_set_nm = '" + codeSetNm + "' AND sys_description = '" + description + "'";
            jdbcTemplate.query(query, (rs) -> {
                while (rs.next()) {
                    id[0] = rs.getString("code");
                }
                return null; //To change body of generated lambdas, choose Tools | Templates.
            });

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return id[0];
    }

    //This method retrieves the FMoH assigned facility ID from the NDR codeset table
    //This ID is used in place of the internal facility ID
    public static FacilityType getFacility(long facilityId) {
        FacilityType facility[] = {new FacilityType()};
        String description[] = {Long.toString(facilityId)};
        try {
            String query = "SELECT * FROM ndrcodeset WHERE sys_description = '" + description[0] + "'";
            jdbcTemplate.query(query, (rs) -> {
                while (rs.next()) {
                    facility[0].setFacilityName(rs.getString("code_description"));
                    facility[0].setFacilityID(rs.getString("code"));
                    facility[0].setFacilityTypeCode("FAC");
                }
                return null; //To change body of generated lambdas, choose Tools | Templates.
            });

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return facility[0];
    }

}
