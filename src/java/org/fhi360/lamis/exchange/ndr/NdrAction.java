/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import com.opensymphony.xwork2.ActionSupport;
import java.util.Arrays;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.service.UploadFolderService;
import org.fhi360.lamis.utility.FileUtil;

/**
 *
 * @author user1
 */
public class NdrAction extends ActionSupport {    
    private String status;
    private String fileName;

    public String dispatcher() {
        NdrConverter converter = new NdrConverter();
        FileUtil fileUtil = new FileUtil();
        UploadFolderService uploadFolderService = new UploadFolderService();
        
        String stateId = ServletActionContext.getRequest().getParameter("stateId");
        String facilityIds = ServletActionContext.getRequest().getParameter("facilityIds");
        System.out.println("Conversion begin state Id..........."+stateId);
        System.out.println("Conversion begin facility Ids..........."+facilityIds);

        if(facilityIds.trim().isEmpty() && stateId.trim().isEmpty()) {
            //Extract all facilities
            String query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT facility_id FROM facility WHERE active = 1 AND datim_id IS NOT NULL AND datim_id != '') ORDER BY facility_id";
            converter.buildMessage(query, true);     
        }
        else {
            //Extract for selected facilities
            if(!facilityIds.trim().isEmpty()) {
                String query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT facility_id FROM facility WHERE facility_id IN (" + facilityIds + ") AND active = 1 AND datim_id IS NOT NULL AND datim_id != '') ORDER BY facility_id";
                converter.buildMessage(query, false);     

//                String facilities[] = facilityIds.split(",");
//                if(!Arrays.toString(facilities).isEmpty() || facilities != null) {
//                    for (int i = 0; i < facilities.length; i++) {
//                        String facilityId = facilities[i];
//                        String folder = ServletActionContext.getServletContext().getInitParameter("contextPath")+ "transfer/temp/" + facilityId + "/";  
//                        fileUtil.makeDir(folder);        
//                        if(uploadFolderService.getUploadFolderStatus(folder).equalsIgnoreCase("unlocked")) {
//                            fileUtil.deleteFileWithExtension(folder, ".xml");
//                            fileUtil.deleteFileWithExtension(folder, ".zip");
//                            uploadFolderService.lockUploadFolder(folder); 
//                            setFileName(converter.buildMessage(Long.parseLong(facilityId)));            
//                            uploadFolderService.unlockUploadFolder(folder); 
//                        }                                
//                    }            
//                }            
            }
            else {
                //Extract for selected State
                if(!stateId.trim().isEmpty()) {
                    String query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT facility_id FROM facility WHERE state_id = " + stateId + " AND active = 1 AND datim_id IS NOT NULL AND datim_id != '') ORDER BY facility_id";
                    converter.buildMessage(query, false);     
                }
            }
        }
        return SUCCESS;        
    }
        
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
