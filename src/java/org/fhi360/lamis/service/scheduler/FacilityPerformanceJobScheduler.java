/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.scheduler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.fhi360.lamis.service.indicator.FacilityPerformanceService;
import org.fhi360.lamis.utility.DateUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author user10
 */
public class FacilityPerformanceJobScheduler  extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
          
        Date now = new Date();
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated        
        if(simpleDateformat.format(now).equalsIgnoreCase("SUN")) {
            // run weekly report
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String weekFirstDate = dateFormat.format(DateUtil.getWeekStartDate(now));
            String weekLastDate = dateFormat.format(DateUtil.getWeekEndDate(now));

        }
        else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            if(cal.get(Calendar.DATE) == cal.getActualMaximum(Calendar.DATE)) {
                // run monthly report
                
               String first = dateFormat.format(DateUtil.getFirstDateOfMonth(now));
               String last = dateFormat.format(DateUtil.getLastDateOfMonth(now));
                
            }
        }
        new FacilityPerformanceService().process();
    }
}
