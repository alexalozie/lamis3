/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.indicator;

import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.PropertyAccessor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.fhi360.lamis.service.beans.ContextProvider;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author user10
 */
public class FacilityPerformanceService {

    private final static JdbcTemplate jdbcTemplate = ContextProvider.getBean(JdbcTemplate.class);
    private final static TransactionTemplate transactionTemplate = ContextProvider.getBean(TransactionTemplate.class);

    public void process() {
        try {

            String query = "";

            String databaseName = new PropertyAccessor().getDatabaseName();
            Map<String, Object> map = new PropertyAccessor().getSystemProperties();
            String appInstance = (String) map.get("appInstance");
            if (appInstance.equalsIgnoreCase("server")) {
                //Select all facilityId that has uploaded data in the last 7 days and run performance analysis
                query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE()";
                if (databaseName.equalsIgnoreCase("h2")) {
                    query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE()";
                }
            } else {
                query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE()";
                if (databaseName.equalsIgnoreCase("h2")) {
                    query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE()";
                }
            }

            query = "SELECT COALESCE(MAX(report_date), '2019-04-01') AS report_date FROM indicatorvalue";
            Date[] sqlDate = {null};

            jdbcTemplate.query(query, resultSet -> {
                sqlDate[0] = resultSet.getDate("report_date");
            });

            query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT DISTINCT facility_id FROM facility WHERE state_id = 3)";

            List<Long> facilityIds = new ArrayList<>();
            Date reportDate = DateUtil.parseStringToDate("2019-04-01", "yyyy-MM-dd");
            jdbcTemplate.query(query, resultSet -> {
                while (resultSet.next()) {
                    facilityIds.add(resultSet.getLong("facility_id"));
                }
                return null;
            });
            ExecutorService executorService = Executors.newFixedThreadPool(10);

            for (Long facilityId : facilityIds) {

                //For every facility generate a db suffix for temporary tables
                String dbSuffix = org.apache.commons.lang.RandomStringUtils.randomAlphabetic(6);
                dbSuffix = dbSuffix + "_" + facilityId;

                ProcessorThread processorThread = new ProcessorThread(facilityId, 
                        sqlDate[0], reportDate, 30, dbSuffix);
                executorService.execute(processorThread);
            }
            executorService.shutdown();
            while (!executorService.isTerminated()) {
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void process(long facilityId, Date reportingDate, String dbSuffix) {
        try {
            System.out.println("Performance Report Started @ " + new Timestamp(new Date().getTime()) + " for facility --- " + facilityId);

            //new ArtIndicatorService().process(facilityId, reportingDate);
            //new HtsIndicatorService().process(facilityId, reportingDate);
            //new DhisIndicatorService().process(facilityId, reportingDate, dbSuffix);
            //new DhisAggregatorService().process(facilityId, reportingDate);

            System.out.println("Performance Report Completed @ " + new Timestamp(new Date().getTime()) + " for facility --- " + facilityId);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    class ProcessorThread implements Runnable {

        private Long facilityId;
        private Date min;
        private Date start;
        private Integer limit;
        private String dbSuffix;

        ProcessorThread(Long facilityId, Date lastUpdate, Date start, Integer limit, String dbSuffix) {
            this.facilityId = facilityId;
            this.min = lastUpdate;
            this.start = start;
            this.limit = limit;
            this.dbSuffix = dbSuffix;
        }

        @Override
        public void run() {
            for (int i = 0; i <= limit; i++) {
                start = DateUtil.addDay(start, 1);
                if (start.compareTo(min) >= 0) {
                    process(facilityId, start, dbSuffix);
                }
            }
        }
    }
}
