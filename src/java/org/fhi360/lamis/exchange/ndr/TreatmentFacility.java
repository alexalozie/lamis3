/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

/**
 *
 * @author user1
 */

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.exchange.ndr.schema.FacilityType;
import org.fhi360.lamis.utility.JDBCUtil;

public class TreatmentFacility {
    private static JDBCUtil jdbcUtil;

    public TreatmentFacility() {
    }

   public static FacilityType getFacility(long facilityId) {
	FacilityType facility = new FacilityType();
        String query = "SELECT * FROM facility WHERE facility_id = " + facilityId;
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
		facility.setFacilityName(rs.getString("name"));
		//facility.setFacilityID(Long.toString(rs.getLong("facility_id")));
		facility.setFacilityID(rs.getString("datim_id"));
		facility.setFacilityTypeCode("FAC");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return facility;
    }
    
}
