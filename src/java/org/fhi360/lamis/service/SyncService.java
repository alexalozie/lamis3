/**
 *
 * @author user1
 */
package org.fhi360.lamis.service;

import java.io.File;
import java.util.Map;
import java.util.Date;
import org.fhi360.lamis.exchange.ndr.NdrConverter;
import org.fhi360.lamis.service.beans.ContextProvider;
import org.fhi360.lamis.service.indicator.FacilityPerformanceService;
import org.fhi360.lamis.utility.FileUtil;
import org.fhi360.lamis.utility.PropertyAccessor;
import org.springframework.jdbc.core.JdbcTemplate;

public class SyncService {

    private final static JdbcTemplate jdbcTemplate = ContextProvider.getBean(JdbcTemplate.class);

    public SyncService() {

    }

    public void startTransaction() {
        //Check if all the facility folders has the lock.ser file
        //If not create a lock.ser file if there are xml files to be consumed
        String facilityIds = getFacilityIds();
        if (!facilityIds.isEmpty()) {
            sync(facilityIds);
        }
    }

    public void lockSyncFolder() {
        //Check if there is any sync folder that has xml files but not locked and lock it to avoid overwrite
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String contextPath = (String) map.get("contextPath");
        File file = null;

        UploadFolderService uploadFolderService = new UploadFolderService();
        try {
            String facilityIds[] = getFacilityIds().split(",");
            for (String facilityId : facilityIds) {
                if (new File(contextPath + "exchange/sync/" + facilityId).exists()) {
                    String folder = contextPath + "exchange/sync/" + facilityId + "/";
                    if (uploadFolderService.getUploadFolderStatus(folder).equalsIgnoreCase("unlocked")) {

                        //check if xml files exist                    
                        boolean found = true;
                        String[] tables = {"monitor", "user", "patient", "clinic", "pharmacy", "laboratory", "statushistory", "regimenhistory"};
                        for (String table : tables) {
                            System.out.println("Handling...Facility: " + facilityId + " started ->  " + table);
                            String fileName = folder + table + ".xml";
                            file = new File(fileName);
                            if (!file.exists()) {
                                found = false;
                                break;
                            }
                        }
                        if (found) {
                            uploadFolderService.lockUploadFolder(folder); //lock folder after writing xml files to database
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sync(String ids) {
        System.out.println("Sync started...at: " + new Date());
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String contextPath = (String) map.get("contextPath");
        File file = null;
        UploadFolderService uploadFolderService = new UploadFolderService();
        FileUtil fileUtil = new FileUtil();
        XmlParserDelegator xmlParserDelegator = new XmlParserDelegator();

        String facilityIds[] = ids.split(",");
        for (String facilityId : facilityIds) {
            try {
                String folder = contextPath + "exchange/sync/" + facilityId + "/";
                //If the folder is locked by the webservice, process the content of the folder
                if (uploadFolderService.getUploadFolderStatus(Long.parseLong(facilityId)).equalsIgnoreCase("locked")) {
                    //Check for process lock file, if not available create else skip
                    System.out.println("Folder Status for facilty ID:" + facilityId + " is: " + uploadFolderService.getUploadFolderSyncingStatus(Long.parseLong(facilityId)));

                    if (uploadFolderService.getUploadFolderSyncingStatus(Long.parseLong(facilityId)).equalsIgnoreCase("unlocked")) {

                        uploadFolderService.lockUploadFolderSyncing(folder);
                        String[] tables = {"monitor", "user", "casemanager", "communitypharm", "patient", "clinic", "pharmacy", "laboratory", "adrhistory", "oihistory", "adherehistory", "statushistory", "regimenhistory", "chroniccare", "dmscreenhistory", "tbscreenhistory", "anc", "delivery", "child", "childfollowup", "maternalfollowup", "partnerinformation", "specimen", "eid", "labno", "nigqual", "devolve", "patientcasemanager", "eac"};
                        for (String table : tables) {
                            String fileName = folder + table + ".xml";
                            file = new File(fileName);
                            if (file.exists()) {
                                System.out.println("Syncing facility: " + facilityId + " table: " + table + " at: " + new Date());
                                System.out.println("Starting file inflation at: " + new Date());
                                fileUtil.inflateFile(fileName, facilityId);  //Inflate the file before parsing
                                System.out.println("Finishing file inflation at: " + new Date());
                                System.out.println("Starting file delegation at: " + new Date());
                                xmlParserDelegator.delegate(table, fileName, "");
                                System.out.println("Finishing file delegation at: " + new Date());
                                System.out.println("Done..." + file);
                                file.delete();
                            }
                        }
                        fileUtil.deleteFile(new File(contextPath + "exchange/sync/" + facilityId));

                        // Run facility performance service after sync
                        new FacilityPerformanceService().process(Long.parseLong(facilityId), new Date(), "");
                    }
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    private void ndr(String ids) {
        System.out.println(".....facilities to generate NDR :" + ids);
        String query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT facility_id FROM facility WHERE facility_id IN (" + ids + ") AND active = 1 AND datim_id IS NOT NULL AND datim_id != '') ORDER BY facility_id";
        new NdrConverter().buildMessage(query, false);
    }

    private String getFacilityIds() {
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String contextPath = (String) map.get("contextPath");
        File directory = new File(contextPath + "exchange/sync/");

        String facilityIds = "";
        if (!directory.exists()) {
            System.out.println("Directory does not exist.");
        } else {
            try {
                String files[] = directory.list();
                for (String facilityId : files) {
                    facilityIds = facilityIds.isEmpty() ? facilityId : facilityIds + "," + facilityId;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Facility Ids: " + facilityIds);
        return facilityIds;
    }

    private long getFacilityCount() {
        long[] count = {0};
        String query = "SELECT MAX(facility_id) AS max_count FROM facility";
        jdbcTemplate.query(query, resultSet -> {
            count[0] = resultSet.getLong("max_count");
        });
        return count[0];
    }
}
