/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service;

import java.util.Date;
import org.fhi360.lamis.service.beans.ContextProvider;
import org.fhi360.lamis.utility.DateUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author user10
 */
public class TreatmentCurrentService {

    private final static JdbcTemplate jdbcTemplate = ContextProvider.getBean(JdbcTemplate.class);
    private final static TransactionTemplate transactionTemplate = ContextProvider.getBean(TransactionTemplate.class);

    public TreatmentCurrentService() {
    }

    public boolean isPatientActive(long patientId, int daysSinceLastRefill, Date reportingDate) {

        String query = "SELECT pharm.date_visit, pharm.duration, pharm.regimentype_id, pharm.regimen_id, regimentype.description AS regimentype, regimen.description AS regimen FROM pharm JOIN regimentype ON pharm.regimentype_id = regimentype.regimentype_id JOIN regimen ON pharm.regimen_id = regimen.regimen_id WHERE pharm.patient_id = " + patientId + " ORDER BY pharm.date_visit DESC LIMIT 1";
        return jdbcTemplate.query(query, resultSet -> {
            Date dateLastRefill = resultSet.getDate("date_visit");
            int duration = resultSet.getInt("duration");
            int monthRefill = duration / 30;
            if (monthRefill <= 0) {
                monthRefill = 1;
            }
            if (dateLastRefill != null) {
                //If the last refill date plus refill duration plus days since last refill  in days is before the last day of the reporting date this patient is Active   
                //or in other words if your 28 days is not after the reporting date your are LTFU
                if (DateUtil.addYearMonthDay(dateLastRefill, duration + daysSinceLastRefill, "DAY").before(reportingDate)) {
                    return false;
                }
            }
            return true;
        });
    }

    public boolean isPatientActiveTLD(long patientId, int daysSinceLastRefill, Date reportingDate) {
        String query = "SELECT pharm.date_visit, pharm.duration, pharm.regimentype_id, pharm.regimen_id, regimentype.description AS regimentype, regimen.description AS regimen FROM pharm JOIN regimentype ON pharm.regimentype_id = regimentype.regimentype_id JOIN regimen ON pharm.regimen_id = regimen.regimen_id WHERE pharm.patient_id = " + patientId + " ORDER BY pharm.date_visit DESC LIMIT 1";
        return jdbcTemplate.query(query, resultSet -> {
            Date dateLastRefill = resultSet.getDate("date_visit");
            int duration = resultSet.getInt("duration");
            String regimen = resultSet.getString("regimen");
            int monthRefill = duration / 30;
            if (monthRefill <= 0) {
                monthRefill = 1;
            }
            if (dateLastRefill != null) {
                //If the last refill date plus refill duration plus days since last refill  in days is before the last day of the reporting date this patient is Active   
                //or in other words if your 28 days is not after the reporting date your are LTFU
                if (DateUtil.addYearMonthDay(dateLastRefill, duration + daysSinceLastRefill, "DAY").before(reportingDate)) {
                    return false;
                } else {
                    if (regimen.contains("TLD")) {
                        return true;
                    }
                }
            }
            return true;
        });
    }

}
