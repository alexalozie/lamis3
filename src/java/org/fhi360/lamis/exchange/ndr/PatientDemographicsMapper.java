/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.exchange.ndr.schema.NoteType;
import org.fhi360.lamis.exchange.ndr.schema.PatientDemographicsType;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user1
 */
public class PatientDemographicsMapper {
    private JDBCUtil jdbcUtil;

    public PatientDemographicsMapper() {
    }
    
    public PatientDemographicsType patientDemograhics(long patientId) {
        PatientDemographicsType patient = new PatientDemographicsType();
        try {
            String query = "SELECT patient_id, facility_id, hospital_num, date_birth, gender, marital_status, education, occupation, state, current_status, date_current_status FROM entity WHERE patient_id = " + patientId;
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String gender = rs.getString("gender") == null ? "" : rs.getString("gender");                
                String maritalStatus =  rs.getString("marital_status") == null ? "" : rs.getString("marital_status");
                String education = rs.getString("education") == null ? "" :rs.getString("education");
                String occupation = rs.getString("occupation") == null ? "" : rs.getString("occupation");
                String state = rs.getString("state") == null ? "" : rs.getString("state");
                String currentStatus = rs.getString("current_status") == null ? "" : rs.getString("current_status");
                String patientIdentifer = rs.getString("hospital_num")+Long.toString(rs.getLong("facility_id"));
                
                patient.setPatientIdentifier(patientIdentifer); //This is the EMR Identifier
                
                if(rs.getString("date_birth") != null && !rs.getString("date_birth").isEmpty()) patient.setPatientDateOfBirth(DateUtil.getXmlDate(rs.getDate("date_birth")));
                if(!gender.isEmpty()) {
                    gender = CodeSetResolver.getCode("SEX", gender);
                    if(!gender.isEmpty()) patient.setPatientSexCode(gender);
                }
                if(currentStatus.equalsIgnoreCase("Known Death")) {
                    patient.setPatientDeceasedIndicator(false);
                    patient.setPatientDeceasedDate(DateUtil.getXmlDate(rs.getDate("date_current_status")));
                }                
                if(!education.isEmpty()) {
                    education = CodeSetResolver.getCode("EDUCATIONAL_LEVEL", education);
                    if(!education.isEmpty()) patient.setPatientEducationLevelCode(education);
                }
                if(!occupation.isEmpty()) {
                    occupation = CodeSetResolver.getCode("OCCUPATION_STATUS", occupation);
                    if(!occupation.isEmpty()) patient.setPatientOccupationCode(occupation);
                }
                if(!maritalStatus.isEmpty()) {
                    maritalStatus = CodeSetResolver.getCode("MARITAL_STATUS", maritalStatus);
                    if(!maritalStatus.isEmpty()) patient.setPatientMaritalStatusCode(maritalStatus);
                }
                if(!state.isEmpty()) {
                    state = CodeSetResolver.getCode("STATES", state);
                    if(!state.isEmpty()) patient.setStateOfNigeriaOriginCode(state);
                }
                patient.setTreatmentFacility(TreatmentFacility.getFacility(rs.getLong("facility_id")));

                //Add a Patient Note
                NoteType patientNote = new NoteType();
                patientNote.setNote("");
                patient.setPatientNotes(patientNote);
            }              
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return patient;        
    }
    
}
