/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.parser.json;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.xml.bind.JAXBException;
import org.fhi360.lamis.dao.hibernate.ChroniccareDAO;
import org.fhi360.lamis.dao.jdbc.ChroniccareJDBC;
import org.fhi360.lamis.dao.jdbc.HtsJDBC;
import org.fhi360.lamis.dao.jdbc.PatientJDBC;
import org.fhi360.lamis.model.Chroniccare;
import org.fhi360.lamis.model.Patient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Idris
 */
public class ChroniccareJsonParser {

    public void parserJson(String table, String content) {
        try {

            JSONObject jsonObj = new JSONObject(content);
            JSONArray jsonArray = jsonObj.optJSONArray(table);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject record = jsonArray.optJSONObject(i);
                Chroniccare chronicCare = getObject(record.toString());
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                long patientId = new PatientJDBC().getPatientId(chronicCare.getFacilityId(), chronicCare.getPatient().getHospitalNum());
                Patient patient = new Patient();
                patient.setPatientId(patientId);
                chronicCare.setPatient(patient);

                long chronicCareId = new ChroniccareJDBC().getChroniccareId(chronicCare.getFacilityId(), patientId, dateFormat.format(chronicCare.getDateVisit()));
                if (chronicCareId == 0L) {
                    ChroniccareDAO.save(chronicCare);
                } else {
                    chronicCare.setChroniccareId(chronicCareId);
                    ChroniccareDAO.update(chronicCare);
                }

            }
        } catch (IOException | JAXBException | JSONException exception) {
            throw new RuntimeException(exception);
        }
    }

    private static Chroniccare getObject(String content) throws JAXBException, JsonParseException, JsonMappingException, IOException {
        Chroniccare chronicCare = new Chroniccare();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
            chronicCare = objectMapper.readValue(content, Chroniccare.class);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        return chronicCare;
    }
}
