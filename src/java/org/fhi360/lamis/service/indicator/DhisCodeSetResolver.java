/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.indicator;

import org.fhi360.lamis.service.beans.ContextProvider;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

public class DhisCodeSetResolver {

    private final static JdbcTemplate jdbcTemplate = ContextProvider.getBean(JdbcTemplate.class);
    private final static TransactionTemplate transactionTemplate = ContextProvider.getBean(TransactionTemplate.class);

    public static String getCode(String codeSetNm, long lamisId) {

        String[] dhisId = {""};
        String query = "SELECT DISTINCT dhis_id FROM dhiscodeset WHERE UPPER(code_set_nm) = '" + codeSetNm.toUpperCase() + "' AND lamis_id = " + lamisId;
        jdbcTemplate.query(query, rs -> {
            dhisId[0] = rs.getString("dhis_id");
        });
        return dhisId[0];
    }
}
