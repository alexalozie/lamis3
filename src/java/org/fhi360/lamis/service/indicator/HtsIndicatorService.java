package org.fhi360.lamis.service.indicator;

import java.util.Date;
import org.fhi360.lamis.service.beans.ContextProvider;
import org.fhi360.lamis.utility.DateUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author user10
 */
public class HtsIndicatorService {

    private long stateId;
    private long lgaId;

    private String query;
    private final static JdbcTemplate jdbcTemplate = ContextProvider.getBean(JdbcTemplate.class);
    private final static TransactionTemplate transactionTemplate = ContextProvider.getBean(TransactionTemplate.class);

    //private static final Log log = LogFactory.getLog(ArtSummaryProcessor.class);
    private int agem1;
    private int agef1;

    private int sumTested, sumPositive, sumInitiated;
    private int dataElementId = 0;

    public HtsIndicatorService() {
    }

    public void process(long facilityId, Date reportingDate) {
        IndicatorPersister indicatorPersister = new IndicatorPersister();
        String reportDate = DateUtil.parseDateToString(reportingDate, "yyyy-MM-dd");
        System.out.println("HtsIndicatorService: running report for : " + reportDate);

        getStateId(facilityId); // stateId and lgaId

        //Compute values for HTS total client tested
        System.out.println("Computing HTS Total Client Tested.....");
        initVariables();

        query = "SELECT DISTINCT hts_id, gender, TIMESTAMPDIFF(YEAR, date_birth, '" + reportDate + "') AS age FROM hts WHERE facility_id = " + facilityId + " AND date_visit = '" + reportDate + "'";
        //  query = "SELECT DISTINCT hts_id, gender FROM hts WHERE facility_id = " + facilityId;

        jdbcTemplate.query(query, resultSet -> {
            while (resultSet.next()) {

                String gender = resultSet.getString("gender");
                //int age = resultSet.getInt("age");

                System.out.println("Gender: " + gender);
                //   System.out.println("Age: " + age);

                if (gender.trim().equalsIgnoreCase("Male")) {
                    agem1++;
                } else {
                    agef1++;
                }
                // disaggregate(gender, age);
            }
            return null;
        });

        //Populate indicatorvalue with values computed for HTS Total client tested
        dataElementId = 101;

        indicatorPersister.persist(dataElementId, 13, stateId, lgaId, facilityId, agem1, reportDate);         // male <1   
        indicatorPersister.persist(dataElementId, 1, stateId, lgaId, facilityId, agef1, reportDate);        //female <1

        // Compute values for HTS total client tested positive
        System.out.println("Computing HTS Total Client Tested Positive.....");
        initVariables();

        query = "SELECT DISTINCT hts_id, gender, hiv_test_result, TIMESTAMPDIFF(YEAR, date_birth, '" + reportDate + "') AS age FROM hts WHERE facility_id = " + facilityId + " AND date_visit = '" + reportDate + "' ";

        jdbcTemplate.query(query, resultSet -> {
            while (resultSet.next()) {
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                String hivResult = resultSet.getString("hiv_test_result");

                if (hivResult.equalsIgnoreCase("Positive")) {
                    if (gender.trim().equalsIgnoreCase("Male")) {
                        agem1++;
                    } else {
                        agef1++;
                    }
                }
                // disaggregate(gender, age);
            }
            return null;
        });

        //Populate indicatorvalue table with values computed for HTS total client tested positive
        dataElementId = 102;

        indicatorPersister.persist(dataElementId, 13, stateId, lgaId, facilityId, agem1, reportDate);         // male <1   
        indicatorPersister.persist(dataElementId, 1, stateId, lgaId, facilityId, agef1, reportDate);        //female <1

        //Compute values for HTS total client enrolled
        System.out.println("Computing HTS Total Client Enrolled.....");
        initVariables();

        query = "SELECT DISTINCT hts_id, gender, date_started, TIMESTAMPDIFF(YEAR, date_birth, '" + reportDate + "') AS age FROM hts WHERE facility_id = " + facilityId + " AND date_visit = '" + reportDate + "' ";

        jdbcTemplate.query(query, resultSet -> {
            while (resultSet.next()) {
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                String artReferred = resultSet.getString("date_started");

                if (artReferred != null && !artReferred.isEmpty()) {
                    if (gender.trim().equalsIgnoreCase("Male")) {
                        agem1++;
                    } else {
                        agef1++;
                    }
                }
                // disaggregate(gender, age);
            }
            return null;
        });
        //Populate the report parameter map with values computed for HTS total client enrolled
        dataElementId = 103;

        indicatorPersister.persist(dataElementId, 13, stateId, lgaId, facilityId, agem1, reportDate);         // male <1   
        indicatorPersister.persist(dataElementId, 1, stateId, lgaId, facilityId, agef1, reportDate);        //female <1

        //Compute values for HTS total client referred by settings
        System.out.println("Computing HTS Total Client Referred by Settings.....");
        initVariables();

        final int[] ct = {0};
        final int[] tb = {0};
        final int[] sti = {0};
        final int[] opd = {0};
        final int[] ward = {0};
        final int[] community = {0};
        final int[] standalone = {0};
        final int[] others = {0};

        query = "SELECT DISTINCT hts_id, gender, testing_setting, TIMESTAMPDIFF(YEAR, date_birth, '" + reportDate + "') AS age FROM hts WHERE facility_id = " + facilityId + " AND date_visit = '" + reportDate + "' ";

        jdbcTemplate.query(query, resultSet -> {
            while (resultSet.next()) {
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                String referredFrom = resultSet.getString("testing_setting");
                if (referredFrom.equalsIgnoreCase("CT")) {
                    ct[0]++;
                }
                if (referredFrom.equalsIgnoreCase("TB")) {
                    tb[0]++;
                }
                if (referredFrom.equalsIgnoreCase("STI")) {
                    sti[0]++;
                }
                if (referredFrom.equalsIgnoreCase("OPD")) {
                    opd[0]++;
                }
                if (referredFrom.equalsIgnoreCase("WARD")) {
                    ward[0]++;
                }
                if (referredFrom.equalsIgnoreCase("Community")) {
                    community[0]++;
                }

                if (referredFrom.equalsIgnoreCase("Standalone Hts")) {
                    standalone[0]++;
                }
                if (referredFrom.equalsIgnoreCase("Others")) {
                    others[0]++;
                }
            }
            return null;
        });

        //Populate the report parameter map with values computed for HTS total client testing settings
        //CT, tb, sti, opd, ward, community, standalone hts, others
        dataElementId = 104;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, ct[0], reportDate);         // ct   
        dataElementId = 105;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, tb[0], reportDate);        //tb
        dataElementId = 106;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, sti[0], reportDate);        //sti
        dataElementId = 107;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, opd[0], reportDate);        //opd
        dataElementId = 108;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, ward[0], reportDate);        //ward
        dataElementId = 109;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, community[0], reportDate);        //community
        dataElementId = 110;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, standalone[0], reportDate);        //standalone hts
        dataElementId = 111;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, others[0], reportDate);        //tb

        System.out.println("Computing HTS Total Client Referred From.....");
        final int[] self = {0};
        tb[0] = 0;
        sti[0] = 0;
        opd[0] = 0;
        final int[] fp = {0};
        ward[0] = 0;
        final int[] blood = {0};
        others[0] = 0;

        query = "SELECT DISTINCT hts_id, gender, referred_from, TIMESTAMPDIFF(YEAR, date_birth, '" + reportDate + "') AS age FROM hts WHERE facility_id = " + facilityId + " AND date_visit = '" + reportDate + "' ";

        jdbcTemplate.query(query, resultSet -> {
            while (resultSet.next()) {
                String referredFrom = resultSet.getString("referred_from");
                if (referredFrom.equalsIgnoreCase("SELF")) {
                    self[0]++;
                }
                if (referredFrom.equalsIgnoreCase("TB")) {
                    tb[0]++;
                }
                if (referredFrom.equalsIgnoreCase("STI")) {
                    sti[0]++;
                }
                if (referredFrom.equalsIgnoreCase("FP")) {
                    fp[0]++;
                }
                if (referredFrom.equalsIgnoreCase("OPD")) {
                    opd[0]++;
                }
                if (referredFrom.equalsIgnoreCase("WARD")) {
                    ward[0]++;
                }
                if (referredFrom.equalsIgnoreCase("Blood bank")) {
                    blood[0]++;
                }
                if (referredFrom.equalsIgnoreCase("Others")) {
                    others[0]++;
                }
            }
            return null;
        });

        //Referred settings
        //self, tb, sti, fp, opd, ward, blood bank, others
        dataElementId = 112;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, self[0], reportDate);
        dataElementId = 113;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, tb[0], reportDate);
        dataElementId = 114;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, sti[0], reportDate);
        dataElementId = 115;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, fp[0], reportDate);
        dataElementId = 116;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, opd[0], reportDate);
        dataElementId = 117;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, ward[0], reportDate);
        dataElementId = 118;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, blood[0], reportDate);
        dataElementId = 119;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, others[0], reportDate);

        //Index testing
        // biological, sexual, social
        final int[] biological = {0};
        final int[] sexual = {0};
        final int[] social = {0};
        query = "SELECT DISTINCT hts_id, gender, type_index, TIMESTAMPDIFF(YEAR, date_birth, '" + reportDate + "') AS age FROM hts WHERE facility_id = " + facilityId + " AND date_visit = '" + reportDate + "' ";

        jdbcTemplate.query(query, rs -> {
            while (rs.next()) {
                String referredFrom = rs.getString("type_index");
                if (referredFrom.equalsIgnoreCase("Biological")) {
                    biological[0]++;
                }
                if (referredFrom.equalsIgnoreCase("Sexual")) {
                    sexual[0]++;
                }
                if (referredFrom.equalsIgnoreCase("Social")) {
                    social[0]++;
                }

            }
            return null;
        });
        dataElementId = 120;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, biological[0], reportDate);
        dataElementId = 121;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, sexual[0], reportDate);
        dataElementId = 122;
        indicatorPersister.persist(dataElementId, 0, stateId, lgaId, facilityId, social[0], reportDate);

        System.out.println("Completed");
    }

    private void executeUpdate(String query) {
        transactionTemplate.execute(ts -> {
            jdbcTemplate.execute(query);
            return null;
        });
    }

    private boolean found(String query) {
        boolean[] found = {false};
        jdbcTemplate.query(query, resultSet -> {
            found[0] = true;
        });
        return found[0];
    }

    private void initVariables() {
        agem1 = 0;
        agef1 = 0;

        sumTested = 0;
        sumPositive = 0;
        sumInitiated = 0;
    }

    private void getStateId(long facilityId) {
        query = "SELECT state_id, lga_id FROM facility  WHERE facility_id = " + facilityId;
        jdbcTemplate.query(query, rs -> {
            stateId = rs.getLong("state_id");
            lgaId = rs.getLong("lga_id");
        });
    }

    private int getCount(String query) {
        int[] count = {0};
        jdbcTemplate.query(query, rs -> {
            count[0] = rs.getInt("count");
        });
        return count[0];
    }

}
